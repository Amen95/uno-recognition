# find_colors, find_shapes and extract_features functions had to be included below again to define the needed and required labels for predicition without getting errors
# I have designed a simple "GUI" that has webcam option and loading images option 
# Importing all necessarily packages
import cv2
import numpy as np
import tkinter as tk
from tkinter import filedialog
from PIL import Image, ImageTk
from joblib import load

# Loading the trained models
color_model_path = "Intelligent Sensing Coursework\\Features Extraction\\color_classification_model.joblib" # adjust the path as needed
shape_model_path = "Intelligent Sensing Coursework\\Features Extraction\\shape_classification_model.joblib" # adjust the path as needed

# Defining the loaded trained models
model_color = load(color_model_path)
model_shape = load(shape_model_path)
    
# Function to extract color features from an image
def find_colors(image):
    hsv_image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    mean_color = cv2.mean(hsv_image)[:3]
    _, std_dev_color = cv2.meanStdDev(hsv_image)  # Extract standard deviation values
    std_dev_color = std_dev_color.flatten()  # Flatten to make it 1D
    color_feature = np.concatenate((mean_color, std_dev_color))
    return color_feature

# Function to extract shape features from an image
def find_shapes(input_contour):
    curvature_threshold = 0.08
    k = 4
    polygon_tolerance = 0.04

    cont_ar = np.asarray(input_contour)

    # Checking if enough points are available for fitting ellipse
    if len(cont_ar) < 5:
        return None  # Return None if not enough points to fit ellipse

    try:
        # Converting contour to 2D array with dtype=float32
        cont_ar = cont_ar.reshape((-1, 1, 2)).astype(np.float32)

        # Computing axes feature
        ellipse = cv2.fitEllipse(cont_ar)
        center, axes, angle = ellipse  # Unpack the ellipse into center, axes, and angle
        majoraxis_length = max(axes)
        minoraxis_length = min(axes)
        axes_ratio = minoraxis_length / majoraxis_length

        area = cv2.contourArea(cont_ar)
        perimeter = cv2.arcLength(cont_ar, True)
        area_ratio = perimeter / area
        perimeter_ratio = minoraxis_length / perimeter

        epsilon = polygon_tolerance * perimeter
        vertex_approx = 1.0 / len(cv2.approxPolyDP(cont_ar, epsilon, True))
        length = len(input_contour)

        # Computing curvature and convexity features
        curvature_chain = []
        for i in range(cont_ar.shape[0] - k):
            num = cont_ar[i][0][1] - cont_ar[i - k][0][1]  # y
            den = cont_ar[i][0][0] - cont_ar[i - k][0][0]  # x
            angle_prev = -np.arctan2(num, den) / np.pi

            num = cont_ar[i + k][0][1] - cont_ar[i][0][1]  # y
            den = cont_ar[i + k][0][0] - cont_ar[i][0][0]  # x
            angle_next = -np.arctan2(num, den) / np.pi

            new_curvature = angle_next - angle_prev
            curvature_chain.append(new_curvature)

        convexity = sum(1 for curvature in curvature_chain if curvature > curvature_threshold)
        concavity = sum(1 for curvature in curvature_chain if curvature < -curvature_threshold)

        convexity_ratio = convexity / len(curvature_chain)
        concavity_ratio = concavity / len(curvature_chain)

        # Define ellipse parameters separately
        ellipse_center = tuple(map(int, center))
        ellipse_axes = tuple(map(int, axes))
        ellipse_angle = int(angle)
        ellipse_center_x, ellipse_center_y = ellipse_center
        ellipse_axis_x, ellipse_axis_y = ellipse_axes

        # Constructing feature vector with all 16 features
        feature_values = [axes_ratio, concavity_ratio, convexity_ratio, area_ratio, vertex_approx, length,
                          perimeter_ratio, ellipse_center[0], ellipse_center[1], ellipse_axes[0], ellipse_axes[1],
                          ellipse_angle, ellipse_center_x, ellipse_center_y, ellipse_axis_x, ellipse_axis_y]
        
        return np.array(feature_values)
    
    except ValueError as e:
        print("Warning:", e)  # Print warning for not enough points to fit ellipse
        return None

# Function to extract features from a selected image
def extract_features(image):
    # Check if the image is from file or webcam
    if isinstance(image, str):  # Image from file
        image = cv2.imread(image)
    else:  # Image from webcam
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)  # Convert RGB to BGR format

    color_feature = find_colors(image)  # Extract color features
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    _, thresholded_image = cv2.threshold(gray_image, 127, 255, cv2.THRESH_BINARY)
    contours, hierarchy = cv2.findContours(thresholded_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    filtered_contours = []
    for i, contour in enumerate(contours):
        if len(contour) == 4:
            continue
        M = cv2.moments(contour)
        if M["m00"] != 0:
            cX = int(M["m10"] / M["m00"])
            cY = int(M["m01"] / M["m00"])
            image_center = (image.shape[1] // 2, image.shape[0] // 2)
            distance_to_center = ((cX - image_center[0]) ** 2 + (cY - image_center[1]) ** 2) ** 0.5
            if distance_to_center < 50:
                if hierarchy[0][i][3] != -1:
                    filtered_contours.append(contour)
    if filtered_contours:
        largest_contour = max(filtered_contours, key=cv2.contourArea)
        shape_feature = find_shapes(largest_contour)  # Extract shape features
        return color_feature, shape_feature
    else:
        return None, None

# Function to browse an image and display predictions
def browse_image():
    # Opening a file dialog to select an image file
    filepath = filedialog.askopenfilename()
    
    # If a file is selected
    if filepath:
        # Extracting features from the selected image
        color_feature, shape_feature = extract_features(filepath)
        
        if color_feature is not None and shape_feature is not None:
            # Reshaping the color feature array for prediction
            color_feature = color_feature.reshape(1, -1)
            
            # Reshaping the shape feature array for prediction
            shape_feature = shape_feature.reshape(1, -1)
            
            # Predicting the color and shape labels using the loaded models
            predicted_color_label = model_color.predict(color_feature)[0]
            predicted_shape_label = model_shape.predict(shape_feature)[0]
            
            # Displaying the predicted color and shape labels along with the image
            display_prediction(filepath, predicted_color_label, predicted_shape_label)
        else:
            print("Error: Unable to extract features from the selected image.")

# Function to display predictiom labels
def display_prediction(image_path, predicted_color_label, predicted_shape_label):
    # Creating a new Tkinter window to display the image with labels
    display_window = tk.Toplevel()
    display_window.title("Image with Labels")
    
    # Loading the image using PIL
    image_pil = Image.open(image_path)
    
    # Converting the image to Tkinter format
    image_tk = ImageTk.PhotoImage(image_pil)
    
    # Creating a label to display the image
    label_image = tk.Label(display_window, image=image_tk)
    label_image.image = image_tk  # Keep a reference to avoid garbage collection
    
    # Creating a label to display the predicted color and shape labels
    label_text = tk.Label(display_window, text=f"Predicted Color: {predicted_color_label}\nPredicted Shape: {predicted_shape_label}", font=("Helvetica", 12))
    
    # Packing the label widgets
    label_image.pack(expand=True, fill='both')
    label_text.pack()

# Function to predict from live webcam
def predict_from_webcam():
    cap = cv2.VideoCapture(1)  # Accessing the default webcam
    
    while True:
        ret, frame = cap.read()  # Reading frame from webcam
        if not ret:
            break
        
        # Extracting features from the frame
        color_feature, shape_feature = extract_features(frame)
        
        if color_feature is not None and shape_feature is not None:
            # Predicting the color and shape labels using the loaded models
            predicted_color_label = model_color.predict(color_feature.reshape(1, -1))[0]
            predicted_shape_label = model_shape.predict(shape_feature.reshape(1, -1))[0]
            
            # Displaying the predicted color and shape labels on the frame
            cv2.putText(frame, f"{predicted_color_label} {predicted_shape_label}", (10, 30),
                        cv2.FONT_HERSHEY_SIMPLEX, 1, (150, 250, 150), 3)
        
        # Displaying the frame
        cv2.imshow('Webcam', frame)
        
        if cv2.waitKey(1) & 0xFF == ord('q'):  # Press 'q' to exit
            break
    
    # Releasing the capture and closing all OpenCV windows
    cap.release()
    cv2.destroyAllWindows()

# Creating a Tkinter window
root = tk.Tk()
root.title("Image Browser")

# Creating buttons to browse an image and predict from webcam
browse_button = tk.Button(root, text="Browse Image", command=browse_image)
browse_button.pack()

webcam_button = tk.Button(root, text="Predict from Webcam", command=predict_from_webcam)
webcam_button.pack()

# Running the Tkinter event loop
root.mainloop()