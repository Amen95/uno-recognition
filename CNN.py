# The code predicts both the colors and shapes of detected cards, but there is a slight error in color classification. It confuses some colors together, except for the "green" color.

# It confuses yellow with blue, red with blue, and blue with yellow, so the dominant color is blue despite trying different augmentation techniques such as brightness and color_shift_ranges, and achieving high training and validation accuracy.

# Importing all necessarily packages 
import cv2
import numpy as np
from tensorflow.keras.models import load_model
import tensorflow as tf
import tensorflow.keras.backend as K # Define the Swish activation function
import logging
logging.getLogger('tensorflow').setLevel(logging.ERROR) # Disable TensorFlow warnings

# I have defined both the swish function and fixeddropout layers because they were used internally in the pre-trained model. I had to include them in the code to avoid getting the deserialization error when loading the saved model.
def swish(x):
    return x * K.sigmoid(x)
# Register Swish as a custom object
tf.keras.utils.get_custom_objects()['swish'] = swish

# Defining the custom layer because it was used internally on the pre trained effientnet B1 model so had to includ it here
class FixedDropout(tf.keras.layers.Layer):
    def __init__(self, rate, seed=None, noise_shape=None, **kwargs):
        super(FixedDropout, self).__init__(**kwargs)
        self.rate = rate
        self.seed = seed
        self.noise_shape = noise_shape

    def call(self, inputs, training=None):
        if training:
            return tf.nn.dropout(inputs, rate=self.rate, seed=self.seed, noise_shape=self.noise_shape)
        return inputs

    def get_config(self):
        config = super(FixedDropout, self).get_config()
        config.update({'rate': self.rate, 'seed': self.seed, 'noise_shape': self.noise_shape})
        return config
# Register the custom layer
tf.keras.utils.get_custom_objects()['FixedDropout'] = FixedDropout

# Loading the trained model without custom objects
model = load_model('Intelligent Sensing Coursework\\CNN Extraction\\Classifier_Model.keras', custom_objects={'FixedDropout': FixedDropout}) # Adjust the path as needed 

# Defining function to preprocess the captured image
def preprocess_image(image):
    # Resize the image to match the input dimensions expected by the model
    resized_image = cv2.resize(image, (224, 224))
    # Normalize pixel values
    normalized_image = resized_image / 255.0
    # Expand dimensions to match the input shape expected by the model
    preprocessed_image = np.expand_dims(normalized_image, axis=0)
    return preprocessed_image

# Defining function to predict color and shape
def predict_color_and_shape(image, class_mapping):
    # Preprocessing the input image
    preprocessed_image = preprocess_image(image)
    # Performing prediction
    prediction = model.predict(preprocessed_image)
    # Getting the predicted class index
    predicted_class_index = np.argmax(prediction)
    # Mapping the predicted class index to the corresponding color and shape
    predicted_color_shape = get_class_name(predicted_class_index, class_mapping)
    return predicted_color_shape

# Function to map class indices to class labels
def get_class_name(index, class_mapping):
    return class_mapping[index]

# Open webcam
cap = cv2.VideoCapture(1)

# Defining the class mapping dictionary
class_mapping = {
    0: 'blue 0', 1: 'blue 1', 2: 'blue 2', 3: 'blue 3', 4: 'blue 4', 5: 'blue 5', 6: 'blue 6', 7: 'blue 7', 8: 'blue 8', 9: 'blue 9',
    10: 'blue draw', 11: 'blue reverse', 12: 'blue skip',
    13: 'green 0', 14: 'green 1', 15: 'green 2', 16: 'green 3', 17: 'green 4', 18: 'green 5', 19: 'green 6', 20: 'green 7', 21: 'green 8', 22: 'green 9',
    23: 'green draw', 24: 'green reverse', 25: 'green skip',
    26: 'red 0', 27: 'red 1', 28: 'red 2', 29: 'red 3', 30: 'red 4', 31: 'red 5', 32: 'red 6', 33: 'red 7', 34: 'red 8', 35: 'red 9',
    36: 'red draw', 37: 'red reverse', 38: 'red skip',
    39: 'wild +2', 40: 'wild +4',
    41: 'yellow 0', 42: 'yellow 1', 43: 'yellow 2', 44: 'yellow 3', 45: 'yellow 4', 46: 'yellow 5', 47: 'yellow 6', 48: 'yellow 7', 49: 'yellow 8', 50: 'yellow 9',
    51: 'yellow draw', 52: 'yellow reverse', 53: 'yellow skip'
}

while True:
    # Capture frame-by-frame
    ret, frame = cap.read()
    
    # Preprocess and predict color and shape
    predicted_color_shape = predict_color_and_shape(frame, class_mapping)
    
    # Display the predicted color and shape on the video frame
    cv2.putText(frame, predicted_color_shape, (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (150, 255, 150), 3)
    
    # Display the captured frame
    cv2.imshow('Webcam', frame)

    # Press 'q' to exit the loop
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Release the capture
cap.release()
cv2.destroyAllWindows()