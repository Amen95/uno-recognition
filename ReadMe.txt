Get Started

Prerequisites
To run this task, you will need:
• Python 3.7 or higher
• OpenCV
• TensorFlow 2.x
• Keras
• Numpy
• Matplotlib
• Joblib
• Tkinter
• sklearn

Installation
• Install Python 3.7 or higher.
• Install OpenCV, Matplotlib, Numpy, TensorFlow, and Keras using pip:

pip install opencv-python matplotlib numpy tensorflow keras
pip install joblib
pip install scikit-learn

Running the Codes for CNN.py, CNN + HSV.py, Features_Extractor.py files
• Install the required packages by running the command pip install -r requirements.txt.
• Open Terminal, navigate to project directory and run CNN only.py then CNN + HSV.py files as explained on video.
• Dataset contains Train where it has 54 classes folders "color shape.
• Run Features_Extractor.py python file and load image using the GUI extractor implemented, try loading the images from "dataset" file inside Features Extraction directory.
• Navigate to CNN_UNO.ipynb Notebook where training and prediction are detailed in cells.
• Saved models for CNN and joblib are included on each directory.
• Capture Uno card images from live camera stream: This option opens the live camera stream to capture the Uno cards used by CNN and CNN + HSV.
• loading Uno card: This option loads the images from path directory using eaither GUI or loaded manually used by Features_Extractor.
